var app = app || {};
app.ui = app.ui || {};
app.ui.yosigasto = (function () {
    function initFormulario() {
        $("#formGasto").on('click', '#guardar', function (e) {
            e.preventDefault();
            var gasto = {};
            gasto.importe = $('#monto').val();
            gasto.fecha = new Date();
            var categoria = $("#categoria").val();
            
            if (!gasto.importe || ($.trim(gasto.importe) === "") || (isNaN($.trim(gasto.importe)))) {
                $('#monto').focus();
                return false;
            } else if (!categoria || ($.trim(categoria) === "") ) {
                $("#categoria").focus();
                return false;
            }            

            var gastosCategoria = JSON.parse(localStorage.getItem(categoria)) || [];
            gastosCategoria.push(gasto);
            localStorage.setItem(categoria, JSON.stringify(gastosCategoria));
            $("#mensaje").removeClass("hide");
            
            setInterval(function(){
                $("#mensaje").addClass("hide");
            },3000);
        });
    }
    function init() {
        initFormulario();

    }
    return{
        init: init
    };
})();
